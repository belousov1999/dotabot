﻿using Source2_compiler.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Source2_compiler
{
    class Program
    {
        //Private Static
        static string dota2Path, addonPath, outPath;
        static DirWalker Walker;

        //Public Static
        public static List<string> CompileableExtensionsList = new List<string> {
            "xml", "css", "js", "png"
        };

        static void Main(string[] args)
        {
            Console.WriteLine("Fetching Arguments");
            FetchArgs(args);
            Console.WriteLine("Walking in content directory");
            Walker = new DirWalker(addonPath);
            Console.WriteLine("Compiling vpk");
            VpkCompile();
            Console.ReadKey();
        }

        private static void FetchArgs(string[] args)
        {
            if (args.Length != 3)
            {
                Console.WriteLine("Usage: Source2_compiler.exe <dota2 path> <addon path> <output directory>");
                Console.ReadKey();
                System.Environment.Exit(1);
                return;
            }

            dota2Path = args[0];
            addonPath = args[1];
            outPath = args[2];

            if (!Directory.Exists(addonPath))
            {
                Console.WriteLine("Output directory doesn`t exist: " + addonPath);
                Console.ReadKey();
                System.Environment.Exit(1);
                return;
            }

            if (!Directory.Exists(outPath))
            {
                Console.WriteLine("Output directory doesn`t exist: " + outPath);
                Console.ReadKey();
                System.Environment.Exit(1);
                return;
            }

            var local_vpkPath = Directory.GetCurrentDirectory() + "\\VPK";
            if (!Directory.Exists(local_vpkPath))
            {
                Console.WriteLine("VPK folder doesn`t exist: " + local_vpkPath);
                Console.ReadKey();
                System.Environment.Exit(1);
                return;
            }
            else
                SystemUtils.DirectoryCopy(local_vpkPath, addonPath);

            var outFile_path = outPath + "\\pak01_dir.vpk";
            if (File.Exists(outFile_path))
            {
                Console.WriteLine("Output file exists. Deleting...");
                File.Delete(outFile_path);
            }
            var gameinfo = Directory.GetCurrentDirectory() + "\\GameInfo\\gameinfo.gi";
            if (File.Exists(gameinfo))
            {
                var original_gameinfo = dota2Path + "game\\dota\\gameinfo.gi";
                File.Copy(gameinfo, original_gameinfo, true);
            }
        }

        private static void VpkCompile()
        {
            var addons_path = outPath + "\\dota_addons";
            if (Directory.Exists(addons_path))
                Directory.Delete(addons_path, true);
            
            foreach (var f in Walker.FilePaths)
            {
                var args = "-f -outroot " + SystemUtils.AddQuotes(outPath) + " -i " + SystemUtils.AddQuotes(f);
                Console.WriteLine("Args: "+args);
                var compiler_p = System.Diagnostics.Process.Start(dota2Path+ "game/bin/win64/resourcecompiler.exe", args);
                compiler_p.WaitForExit();
            }
            var vpk_zipper = Directory.GetCurrentDirectory() + "\\vpk_zipper";
            if (!Directory.Exists(vpk_zipper))
            {
                Console.WriteLine("vpk_zipper folder doesn`t exist: " + vpk_zipper);
                Console.ReadKey();
                System.Environment.Exit(1);
            }
            var zipper_p = new Process();
            zipper_p.StartInfo.UseShellExecute = true;
            zipper_p.StartInfo.FileName = vpk_zipper + "\\vpk.exe";
            zipper_p.StartInfo.Arguments = SystemUtils.AddQuotes(addons_path + "\\fusion");
            zipper_p.StartInfo.CreateNoWindow = true;
            zipper_p.Start();
            zipper_p.WaitForExit();

            if (!File.Exists(addons_path + "\\fusion.vpk"))
            {
                Console.WriteLine("VPK file not created in dota_addons folder: "+ addons_path);
                Console.ReadKey();
                System.Environment.Exit(1);
            }
            File.Copy(addons_path + "\\fusion.vpk", outPath + "\\pak01_dir.vpk");
            if (Directory.Exists(addons_path))
                Directory.Delete(addons_path, true);
            Console.WriteLine("Vpk compiled");
        }
    }
}
