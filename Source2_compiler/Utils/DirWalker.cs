﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Source2_compiler.Utils
{
    public class DirWalker
    {
        public List<string> FilePaths = new List<string>();

        public DirWalker(string path)
        {
            Walk(path);
        }

        private void Walk(string path)
        {
            foreach (var d in Directory.GetDirectories(path))
                Walk(d);
            foreach (var f in Directory.GetFiles(path))
                FilePaths.Add(f);
        }
    }
}
