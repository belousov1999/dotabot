var data;
function Start() {
    Fusion.OnTick.push(Update);
}

class Vector2 {
    x: number;
    y: number;
    constructor(_x: number, _y: number) {
        this.x = _x;
        this.y = _y;
    }
}

class Unit {
    name: string;
    worldPos: number[];
    screenPos: Vector2;
    hp: number;
    mp: number;
    isHero: boolean;
    abilities: string[];

    constructor(ent: Entity, _screenPos: Vector2) {
        this.name = ent.UnitName;
        this.worldPos = ent.AbsOrigin.Common;
        this.screenPos = _screenPos;
        this.hp = ent.HealthPercent;
        this.mp = ent.Mana;
        this.isHero = ent.IsHero;
        var s = []
        for (var k in ent.Abilities)
            if (ent.Abilities[k].AbilityName.indexOf("seasonal") < 0)
                s.push(ent.Abilities[k].AbilityName);
        this.abilities = s;
    }
}

class SendData {
    hero: Unit;
    units: Unit[];
    mousePos: Vector2;
    cameraPos: Vector2;
    constructor(_hero: Unit, _units: Unit[], _mousePos: Vector2, _cameraPos: Vector2) {
        this.hero = _hero;
        this.units = _units;
        this.mousePos = _mousePos;
        this.cameraPos = _cameraPos;
    }
}

function GetScreenPos(pos: number[]) {
    
    return [
        Game.WorldToScreenX(pos[0], pos[1], pos[2]),
        Game.WorldToScreenY(pos[0], pos[1], pos[2])]
}

function IsValidScreenPos(pos: Vector2): boolean {
    var valid_x = pos.x > 0 && pos.x < Game.GetScreenWidth();
    var valid_y = pos.y > 200 && pos.y < Game.GetScreenHeight() - 200;
    return valid_x && valid_y;
}

function PrepareData() {
    try {
        var units: Unit[] = [];
        var hero: Unit;
        var mousePos: Vector2;
        var ents = EntityManager.GetAllUnitsInRange(EntityManager.MyEnt, 2000);
        var myent = EntityManager.MyEnt;
        for (var k in ents) {
            var ent = ents[k];
            if (ent.UnitName.length > 0 && myent.id !== ent.id) {
                var pos = GetScreenPos(ent.AbsOrigin.Common);
                var screenPos = new Vector2(pos[0], pos[1]);

                if (IsValidScreenPos(screenPos))
                    units.push(new Unit(ent, screenPos));
            }
        }
        var pos = GetScreenPos(myent.AbsOrigin.Common);
        var screenPos = new Vector2(pos[0], pos[1]);
        hero = new Unit(myent, screenPos);
        var cursorPos = GameUI.GetCursorPosition();
        mousePos = new Vector2(cursorPos[0], cursorPos[1]);
        var data = new SendData(hero, units, mousePos, GameUI.GetScreenWorldPosition());
        $.Msg_old("Data: " + JSON.stringify(data));
    } catch (e) {
        $.Msg_old("[PrepareData]: " + e);
    }
}



var precached = false;
function Update() {

    if (GameUI.IsMouseDown(0)) {
        //$.Msg_old("Button pressed 0: " + GameUI.GetScreenWorldPosition())

    }
    if (GameUI.IsMouseDown(1)) {
        // $.Msg_old("Button pressed 1: " + GameUI.GetCursorPosition());

        PrepareData();
    }
    if (GameUI.IsMouseDown(2)) {
        $.Msg_old("Button pressed 2: ")
    }
}

module = {
    name: "Bot",
    onToggle: checkbox => {
        if (checkbox.checked) {
            Start();
            Utils.ScriptLogMsg("Script enabled: Bot", "#00ff00")
        } else {
            Fusion.OnTick.remove(Update)
            ItemPanelHM = []
            if (Fusion.Panels.ItemPanel) {
                Fusion.Panels.ItemPanel.DeleteAsync(0)
                delete Fusion.Panels.ItemPanel
            }
            Utils.ScriptLogMsg("Script disabled: Bot", "#ff0000")
        }
    },
    onDestroy: () => {
        Fusion.OnTick.remove(Update);
    }
}